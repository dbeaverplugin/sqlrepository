/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views.serialization;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.google.gson.Gson;

import pl.zp.sql.repository.views.Consts;
import pl.zp.sql.repository.views.model.TreeObject;
import pl.zp.sql.repository.views.model.TreeParent;
import pl.zp.sql.repository.views.serialization.gson.TreeParentSerializer;

public class ReadFromJson {

	public ReadFromJson() {

	}

	public TreeObject read() {

		Bundle bundle = FrameworkUtil.getBundle(getClass());
		IPath home = Platform.getStateLocation(bundle);
		try {
			Path filePath = Paths.get(home.toString(), Consts.SQL_JSON);
			TreeParent model = null;
			if (Files.isRegularFile(filePath)) {
				try (Reader reader = Files.newBufferedReader(filePath, StandardCharsets.UTF_8)) {
					Gson gson = TreeParentSerializer.createGson();
					model = gson.fromJson(reader, TreeParent.class);
				}
			}
			return model;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

}
