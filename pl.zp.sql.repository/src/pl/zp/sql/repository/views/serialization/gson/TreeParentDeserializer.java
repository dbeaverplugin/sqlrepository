package pl.zp.sql.repository.views.serialization.gson;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import pl.zp.sql.repository.views.Consts;
import pl.zp.sql.repository.views.model.TreeObject;
import pl.zp.sql.repository.views.model.TreeParent;

public class TreeParentDeserializer implements JsonDeserializer<TreeParent> {

	@Override
	public TreeParent deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		JsonObject jsonObject = arg0.getAsJsonObject();
		JsonElement nameElement = jsonObject.get(Consts.ATTRIBUTE_NAME);
		TreeParent treeParent = new TreeParent(nameElement.getAsString());
		JsonElement childrenElement = jsonObject.get(Consts.ATTRIBUTE_CHILDREN);
		Gson gson = TreeParentSerializer.createGson();
		for (JsonElement ele : childrenElement.getAsJsonArray()) {
			JsonObject e = ele.getAsJsonObject();
			JsonElement typeElement = e.get(Consts.ATTRIBUTE_TYPE);
			TreeObject child = null;
			if (Consts.OBJECT_TREEPARENT.equals(typeElement.getAsString())) {
				child = gson.fromJson(ele, TreeParent.class);
			} else if (Consts.OBJECT_TREEOBJECT.equals(typeElement.getAsString())) {
				child = gson.fromJson(ele, TreeObject.class);
			}
			treeParent.addChild(child);

		}

		return treeParent;
	}

}
