/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views.serialization.gson;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import pl.zp.sql.repository.views.Consts;
import pl.zp.sql.repository.views.model.TreeObject;

public class TreeObjectDeserializer implements JsonDeserializer<TreeObject> {

	@Override
	public TreeObject deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		JsonObject jsonObject = arg0.getAsJsonObject();
		JsonElement nameElement = jsonObject.get(Consts.ATTRIBUTE_NAME);
		JsonElement sqlElement = jsonObject.get(Consts.ATTRIBUTE_SQL);
		return new TreeObject(nameElement.getAsString(), sqlElement.getAsString());
	}

}
