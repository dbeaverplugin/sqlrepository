/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views.serialization;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import pl.zp.sql.repository.views.Consts;
import pl.zp.sql.repository.views.model.TreeObject;
import pl.zp.sql.repository.views.serialization.gson.TreeParentSerializer;

public class SaveToJson {

	private TreeObject model;

	public SaveToJson(TreeObject model, IProgressMonitor monitor) {
		this.model = model;
	}

	public void save() {

		Bundle bundle = FrameworkUtil.getBundle(getClass());
		IPath home = Platform.getStateLocation(bundle);
		Path dir = Paths.get(home.toString());
		try {
			if (!Files.isDirectory(dir)) {
				Files.createDirectory(dir);
			}
			Path filePath = Paths.get(home.toString(), Consts.SQL_JSON);
			try (Writer writer = Files.newBufferedWriter(filePath, StandardCharsets.UTF_8)) {
				Gson gson = TreeParentSerializer.createGson();
				JsonElement st = gson.toJsonTree(model);
				gson.toJson(st, writer);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
