/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views.serialization.gson;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import pl.zp.sql.repository.views.Consts;
import pl.zp.sql.repository.views.model.TreeObject;

public class TreeObjectSerializer implements JsonSerializer<TreeObject> {

	@Override
	public JsonElement serialize(TreeObject arg0, Type arg1, JsonSerializationContext arg2) {
		JsonObject jsonElement = new JsonObject();

		jsonElement.add(Consts.ATTRIBUTE_NAME, new JsonPrimitive(arg0.getName()));
		jsonElement.add(Consts.ATTRIBUTE_SQL, new JsonPrimitive(arg0.getSql()));
		jsonElement.add(Consts.ATTRIBUTE_TYPE, new JsonPrimitive(Consts.OBJECT_TREEOBJECT));
		return jsonElement;
	}

}
