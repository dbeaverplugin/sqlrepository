/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.google.gson.Gson;

import pl.zp.sql.repository.views.Consts;
import pl.zp.sql.repository.views.serialization.ReadFromJson;
import pl.zp.sql.repository.views.serialization.gson.TreeParentSerializer;

public class BuildModel {

	private TreeParent root;

	public BuildModel(TreeParent root) {
		this.root = root;
	}

	public TreeObject model() {
		buildModel();

		return root;
	}

	private void buildModel() {
		if (!readFromFile()) {
			try {
				readFromRemote();
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		}

	}

	private void readFromRemote() throws IOException, URISyntaxException {
		Bundle bundle = FrameworkUtil.getBundle(getClass());
		URL file = bundle.getResource(Consts.JSON_SQL_JSON);

		try (InputStream in = file.openStream(); Reader reader = new InputStreamReader(in)) {
			Gson gson = TreeParentSerializer.createGson();
			root = gson.fromJson(reader, TreeParent.class);
		}
//		
//		TreeObject to1 = new TreeObject("How much space is taken", "SELECT\r\n" + "    pg_database.datname,\r\n"
//				+ "    pg_size_pretty(pg_database_size(pg_database.datname)) AS size\r\n" + "    FROM pg_database;");
//		TreeObject to2 = new TreeObject("How much space is taken - detailed", "SELECT\r\n"
//				+ "   relname as \"Table\",\r\n" + "   pg_size_pretty(pg_total_relation_size(relid)) As \"Size\",\r\n"
//				+ "   pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) as \"External Size\"\r\n"
//				+ "   FROM pg_catalog.pg_statio_user_tables ORDER BY pg_total_relation_size(relid) DESC;");
//		TreeObject to3 = new TreeObject("Size of all objects", "SELECT\r\n" + 
//				"   relname AS objectname,\r\n" + 
//				"   relkind AS objecttype,\r\n" + 
//				"   reltuples AS \"#entries\", pg_size_pretty(relpages::bigint*8*1024) AS size\r\n" + 
//				"   FROM pg_class\r\n" + 
//				"   WHERE relpages >= 8\r\n" + "   ORDER BY relpages DESC;");
//		TreeParent p1 = new TreeParent("PostgreSQL");
//		p1.addChild(to1);
//		p1.addChild(to2);
//		p1.addChild(to3);
//
//
//		TreeParent p2 = new TreeParent("Oracle");
//		root.addChild(p1);
//		root.addChild(p2);
	}

	private boolean readFromFile() {

		ReadFromJson rfj = new ReadFromJson();
		TreeObject model = rfj.read();
		if (model != null) {
			root = (TreeParent) model;
			return true;
		}
		return false;
	}

}
