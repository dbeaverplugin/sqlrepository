/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views;

import org.eclipse.jface.resource.ImageDescriptor;

import pl.zp.sql.ActivatorSQLRepository;

public class ImageTool {

	public static ImageDescriptor createImageFolderAdd() {
		return ActivatorSQLRepository.imageDescriptorFromPlugin("pl.zp.sql.repository", "icons/folder-add.png");
	}

	public static ImageDescriptor createImageFolderEdit() {
		return ActivatorSQLRepository.imageDescriptorFromPlugin("pl.zp.sql.repository", "icons/folder-edit.png");
	}

	public static ImageDescriptor createImageFolderRemove() {
		return ActivatorSQLRepository.imageDescriptorFromPlugin("pl.zp.sql.repository", "icons/folder-remove.png");
	}

	public static ImageDescriptor createImageSQLAdd() {
		return ActivatorSQLRepository.imageDescriptorFromPlugin("pl.zp.sql.repository", "icons/sql.png");
	}

	public static ImageDescriptor createImageSQLEdit() {
		return ActivatorSQLRepository.imageDescriptorFromPlugin("pl.zp.sql.repository", "icons/edit-sql-query.png");
	}

	public static ImageDescriptor createImageSQLRemove() {
		return ActivatorSQLRepository.imageDescriptorFromPlugin("pl.zp.sql.repository", "icons/delete-sql-query.png");
	}

	public static ImageDescriptor createImageFolder() {
		return ActivatorSQLRepository.imageDescriptorFromPlugin("pl.zp.sql.repository", "icons/folder-1.png");
	}

}
