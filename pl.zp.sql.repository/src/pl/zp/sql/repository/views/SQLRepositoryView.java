/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;

import pl.zp.sql.repository.views.dialog.SQLEditDialog;
import pl.zp.sql.repository.views.dialog.SQLEditGroupDialog;
import pl.zp.sql.repository.views.model.BuildModel;
import pl.zp.sql.repository.views.model.TreeObject;
import pl.zp.sql.repository.views.model.TreeParent;
import pl.zp.sql.repository.views.serialization.SaveToJson;

public class SQLRepositoryView extends ViewPart implements ISaveablePart {

//	@Override
//	public void init(IViewSite site, IMemento memento) throws PartInitException {
//		super.init(site, memento);
//	}
//
//	@Override
//	public void saveState(IMemento memento) {
//		super.saveState(memento);
//	}

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "pl.zp.sql.repository.views.SQLRepositoryView";

	@Inject
	IWorkbench workbench;

	private TreeViewer viewer;
	private DrillDownAdapter drillDownAdapter;
	private Action action1;
	private Action action2;
	private Action doubleClickAction;

	private boolean dirty = false;

	public TreeObject model;

	class ViewContentProvider implements ITreeContentProvider {
		private TreeParent invisibleRoot;

		public Object[] getElements(Object parent) {
			if (parent.equals(getViewSite())) {
				if (invisibleRoot == null)
					initialize();
				return getChildren(invisibleRoot);
			}
			return getChildren(parent);
		}

		public Object getParent(Object child) {
			if (child instanceof TreeObject) {
				return ((TreeObject) child).getParent();
			}
			return null;
		}

		public Object[] getChildren(Object parent) {
			if (parent instanceof TreeParent) {
				return ((TreeParent) parent).getChildren();
			}
			return new Object[0];
		}

		public boolean hasChildren(Object parent) {
			if (parent instanceof TreeParent)
				return ((TreeParent) parent).hasChildren();
			return false;
		}

		/*
		 * We will set up a dummy model to initialize tree heararchy. In a real code,
		 * you will connect to a real model and expose its hierarchy.
		 */
		private void initialize() {

			TreeParent root = new TreeParent("Repository");
			BuildModel bm = new BuildModel(root);
			invisibleRoot = new TreeParent("");
			model = bm.model();
			invisibleRoot.addChild(model);
		}
	}

	class ViewLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			return obj.toString();
		}

		public Image getImage(Object obj) {

			if (obj instanceof TreeParent) {
				return ImageTool.createImageFolder().createImage();
			} else {
				return ImageTool.createImageSQLAdd().createImage();
			}
		}
	}

	@Override
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);

		viewer.setContentProvider(new ViewContentProvider());
		viewer.setInput(getViewSite());
		viewer.setLabelProvider(new ViewLabelProvider());

		// Create the help context id for the viewer's control
//		workbench.getHelpSystem().setHelp(viewer.getControl(), "pl.zp.sql.repository.viewer");
//		getSite().setSelectionProvider(viewer);
//		makeActions();
		hookContextMenu();
//		hookDoubleClickAction();
//		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);

		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(manager -> {

//			IAction editorAction = new Action("Open in SQL console",
//					DBeaverIcons.getImageDescriptor(UIIcon.SQL_CONSOLE)) {
//				@Override
//				public void run() {
//					try {
//						openSelectionInEditor();
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
//			};
			IAction copyAction = new Action("Copy to clippboard") {
				@Override
				public void run() {
					copySelectionToClipboard(false);
				}
			};
			IAction editAction = new Action("Edit", ImageTool.createImageSQLEdit()) {
				@Override
				public void run() {
					editSelection(false);
				}
			};
			IAction deleteAction = new Action("Delete", ImageTool.createImageSQLRemove()) {
				@Override
				public void run() {
					deleteSelection(false);
				}
			};
			copyAction.setActionDefinitionId(IWorkbenchCommandConstants.EDIT_COPY);

			boolean hasStatements = false;
			if (viewer.getStructuredSelection().getFirstElement() != null) {
				ITreeSelection a = viewer.getStructuredSelection();
				for (Object ob : a.toArray()) {
					TreeObject to = (TreeObject) ob;
					if (to.getSql() != null) {
						hasStatements = true;
						break;
					}
				}
			}

			if (hasStatements) {
//				manager.add(editorAction);
//				manager.add(new Separator());
				manager.add(copyAction);
				manager.add(new Separator());
				manager.add(editAction);
				manager.add(deleteAction);
			} else {
				IAction addAction = new Action("Add sql", ImageTool.createImageSQLAdd()) {
					@Override
					public void run() {
						addSelection(false);
					}
				};

				IAction addGroupAction = null;
				addGroupAction = new Action("Add folder", ImageTool.createImageFolderAdd()) {
					@Override
					public void run() {
						addGroupSelection(false);
					}
				};

				IAction editGroupAction = new Action("Edit folder", ImageTool.createImageFolderEdit()) {
					@Override
					public void run() {
						editGroupSelection(false);
					}
				};
				IAction deleteGroupAction = new Action("Delete folder", ImageTool.createImageFolderRemove()) {
					@Override
					public void run() {
						deleteGroupSelection(false);
					}
				};
				manager.add(addAction);
				manager.add(new Separator());
				manager.add(addGroupAction);
				manager.add(editGroupAction);
				manager.add(deleteGroupAction);

			}
		});
		menuMgr.setRemoveAllWhenShown(true);

		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(action1);
		manager.add(new Separator());
		manager.add(action2);
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(action1);
		manager.add(action2);
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
	}

	private void makeActions() {
		action1 = new Action() {
			public void run() {
				showMessage("Action 1 executed");
			}
		};
		action1.setText("Action 1");
		action1.setToolTipText("Action 1 tooltip");
		action1.setImageDescriptor(
				PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		action2 = new Action() {
			public void run() {
				showMessage("Action 2 executed");
			}
		};
		action2.setText("Action 2");
		action2.setToolTipText("Action 2 tooltip");
		action2.setImageDescriptor(workbench.getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		doubleClickAction = new Action() {
			public void run() {
				IStructuredSelection selection = viewer.getStructuredSelection();
				Object obj = selection.getFirstElement();
				showMessage("Double-click detected on " + obj.toString());
			}
		};
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(), "SQL Repository", message);
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public void copySelectionToClipboard(boolean extraInfo) {
		TreeObject tdt = getSelectedObject(extraInfo);
		if (tdt == null || "".equals(tdt.getSql())) {
			return;
		}

		if (tdt.getSql() != null && tdt.getSql().length() > 0) {
//			UIUtils.setClipboardContents(viewer.getTree().getDisplay(), TextTransfer.getInstance(), tdt.getSql());

			Clipboard clipboard = new Clipboard(viewer.getTree().getDisplay());
			clipboard.setContents(new Object[] { tdt.getSql() }, new Transfer[] { TextTransfer.getInstance() });
			clipboard.dispose();
		}
	}

	public void addSelection(boolean extraInfo) {
		TreeParent tdt = getSelectedParent(extraInfo);
		if (tdt == null) {
			return;
		}

		TreeObject to = new TreeObject("", "");
		SQLEditDialog dialog = new SQLEditDialog(viewer.getTree().getShell(), to.getName(), to.getSql());
		dialog.create();
		if (dialog.open() == Window.OK) {
			to.setName(dialog.getDescription());
			to.setSql(dialog.getSql());
			tdt.addChild(to);
			viewer.refresh(tdt, true);
			dirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}

	public void editSelection(boolean extraInfo) {
		TreeObject tdt = getSelectedObject(extraInfo);
		if (tdt == null) {
			return;
		}

		SQLEditDialog dialog = new SQLEditDialog(viewer.getTree().getShell(), tdt.getName(), tdt.getSql());
		dialog.create();
		if (dialog.open() == Window.OK) {
			tdt.setName(dialog.getDescription());
			tdt.setSql(dialog.getSql());
			viewer.refresh(tdt, true);
			dirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}

	public void deleteSelection(boolean extraInfo) {
		TreeObject tdt = getSelectedObject(extraInfo);
		if (tdt == null) {
			return;
		}

		boolean result = MessageDialog.openConfirm(viewer.getTree().getShell(), "Confirm ",
				"Remove " + tdt.getName() + " ?");
		if (result) {
			viewer.remove(tdt);
			dirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}

	public void addGroupSelection(boolean extraInfo) {
		TreeParent tdt = getSelectedParent(extraInfo);
		if (tdt == null) {
			return;
		}

		TreeParent tdtNew = new TreeParent("");
		SQLEditGroupDialog dialog = new SQLEditGroupDialog(viewer.getTree().getShell(), tdtNew.getName());
		dialog.create();
		if (dialog.open() == Window.OK) {
			tdt.addChild(tdtNew);
			tdtNew.setName(dialog.getDescription());
			viewer.refresh(tdt, true);
			dirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}

	public void editGroupSelection(boolean extraInfo) {
		TreeObject tdt = getSelectedObject(extraInfo);
		if (tdt == null) {
			return;
		}

		SQLEditGroupDialog dialog = new SQLEditGroupDialog(viewer.getTree().getShell(), tdt.getName());
		dialog.create();
		if (dialog.open() == Window.OK) {
			tdt.setName(dialog.getDescription());
			viewer.refresh(tdt, true);
			dirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}

	public void deleteGroupSelection(boolean extraInfo) {
		TreeObject tdt = getSelectedObject(extraInfo);
		if (tdt == null) {
			return;
		}

		boolean result = MessageDialog.openConfirm(viewer.getTree().getShell(), "Confirm ",
				"Remove " + tdt.getName() + " ?");
		if (result) {
			viewer.remove(tdt);
			dirty = true;
			firePropertyChange(PROP_DIRTY);
		}
	}

	private TreeObject getSelectedObject(boolean extraInfo) {
		TreeObject firstElement = (TreeObject) viewer.getStructuredSelection().getFirstElement();
		if (firstElement == null) {
			return null;
		} else {
			return firstElement;
		}
	}

	private TreeParent getSelectedParent(boolean extraInfo) {
		TreeParent firstElement = (TreeParent) viewer.getStructuredSelection().getFirstElement();
		if (firstElement == null) {
			return null;
		} else {
			return firstElement;
		}
	}

//	private void openSelectionInEditor() throws InterruptedException {
//		DBPDataSourceContainer dsContainer = null;
//		StringBuilder sql = new StringBuilder();
//		TreeObject firstElement = (TreeObject) viewer.getStructuredSelection().getFirstElement();
//		if (firstElement != null) {
//			sql.append(firstElement.getSql());
//			if (sql.length() > 0) {
//				dsContainer = podajDomyslny();
//				OpenHandler.openSQLConsole(UIUtils.getActiveWorkbenchWindow(), dsContainer, "Opis", sql.toString());
//			}
//		}
//
//	}
//
//	private DBPDataSourceContainer podajDomyslny() throws InterruptedException {
//
//		IWorkbenchPart activePart = getSite().getWorkbenchWindow().getActivePage().getActivePart();
//		if (activePart instanceof IDataSourceContainerProvider) {
//			return ((IDataSourceContainerProvider) activePart).getDataSourceContainer();
//		}
//		return getCurrentConnection(null);
//
//	}
//
//	@Nullable
//	private DBPDataSourceContainer getCurrentConnection(ExecutionEvent event) throws InterruptedException {
//		DBPDataSourceContainer dataSourceContainer = null;// getDataSourceContainer(event, false);
//		final ProjectRegistry projectRegistry = DBeaverCore.getInstance().getProjectRegistry();
//		IProject project = dataSourceContainer != null ? dataSourceContainer.getRegistry().getProject()
//				: projectRegistry.getActiveProject();
//
//		if (dataSourceContainer == null) {
//			final DataSourceRegistry dataSourceRegistry = projectRegistry.getDataSourceRegistry(project);
//			if (dataSourceRegistry == null) {
//				return null;
//			}
//			if (dataSourceRegistry.getDataSources().size() == 1) {
//				dataSourceContainer = dataSourceRegistry.getDataSources().get(0);
//			} else if (!dataSourceRegistry.getDataSources().isEmpty()) {
//				SelectDataSourceDialog dialog = new SelectDataSourceDialog(
//						workbench.getActiveWorkbenchWindow().getShell(), project, null);
//				if (dialog.open() == IDialogConstants.CANCEL_ID) {
//					throw new InterruptedException();
//				}
//				dataSourceContainer = dialog.getDataSource();
//			}
//		}
//		return dataSourceContainer;
//	}
//
//	public DBPDataSourceContainer getDataSourceContainer(IWorkbenchPart activePart) {
//		if (activePart instanceof IDataSourceContainerProvider) {
//			return ((IDataSourceContainerProvider) activePart).getDataSourceContainer();
//		}
//		if (activePart instanceof DBPContextProvider) {
//			DBCExecutionContext context = ((DBPContextProvider) activePart).getExecutionContext();
//			return context == null ? null : context.getDataSource().getContainer();
//		}
//		return null;
//	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			monitor.beginTask("Saving sql's", 100);
			new SaveToJson(model, monitor).save();
			dirty = false;
			firePropertyChange(PROP_DIRTY);
		} finally {
			monitor.done();
		}
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public boolean isSaveOnCloseNeeded() {
		return true;
	}

}
