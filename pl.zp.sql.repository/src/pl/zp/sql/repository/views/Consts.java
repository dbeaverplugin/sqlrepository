/*
 * SQLRepository - DBeaver plugin
 * Copyright 2018 Zbigniew Powroźnik
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.zp.sql.repository.views;

public class Consts {
	public static final String SQL_JSON = "SQL.json";
	public static final String JSON_SQL_JSON = "json/" + SQL_JSON;

	public static final String ATTRIBUTE_SQL = "sql";
	public static final String ATTRIBUTE_NAME = "name";
	public static final String ATTRIBUTE_TYPE = "type";
	public static final String ATTRIBUTE_CHILDREN = "children";

	public static final String OBJECT_TREEOBJECT = "to";
	public static final String OBJECT_TREEPARENT = "tp";

}
